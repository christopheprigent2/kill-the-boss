const {
  contextBridge,
  ipcRenderer
}                     = require("electron");
const { Client }      = require("pg");

// import LogRocket from 'logrocket';
// LogRocket.init('z3zz4t/kill-the-boss');

// globals => conf
var G_verbose         = 0 // 0 : mute, 3 : verbose++

// const utils = require("../public/shared/utils.js")
// d('calc:')
// d(utils.calcMove('0-0','RIGHT'))
var password          = 'emR-gXeZPvNI2DyU1hIPmA'
const host            = 'sonic-boxer-13313.8nj.gcp-europe-west1.cockroachlabs.cloud'
const port            = 26257
const client = new Client({
  host                : host,
  port                : port,
  database            : "ktb",
  user                : "supert0f",
  password            : password,
  ssl                 : true
});
client.connect();
function d(data) {
  if (G_verbose > 1 && data)
      console.log(data)
}
/**
 * getPlayer
 * 
 * @param {string} login to retrieve. default : retrieve all players
 * @returns a promise with query result
 */
function getPlayer(login=false) {
  let query           = `
select
  p.login,
  p.avatar,
  p.hp,
  p.items,
  cp.position,
  cp.world
from
  players p
  left outer join cells_players cp
on
	p.login              = cp.login`;


  if (login!=false)
    query             += `  AND p.login='${login}';`;
  else
    query             += ';';
  d(`getPlayer query ${query}`)
  return client.query(query)
}

function getPlayers() {
  return getPlayer(false)
}

function getWorld(world = 'world_spawn') {
  let query           = `
select
  cp."login",
  c.position,
	c.items,
	c.properties,
	c.world,
	c.mobs
from
	cells c
left outer join cells_players cp
on
	c.position           = cp.position
and c.world           = '${world}'
order by
position;`;
  d(`getWorld query ${query}`)
  return client.query(query)
}

async function movePlayer(player, oldPosition, direction) {
  d(`movePlayer(${player}, ${oldPosition}, ${direction})`)
  var world           = 'world_spawn'
  newPosition         = calcMove(oldPosition,direction)
  query               = `update cells_players
  set position        = '${newPosition}'
  where "login"       = '${player}';`
  d(`movePlayer(${player}, ${direction}), oldPosition : ${oldPosition}, new ${newPosition}: 
  ${query}`)

  await client.query(query)
  return newPosition
}

contextBridge.exposeInMainWorld('versions', {
  node                : () => process.versions.node,
  chrome              : () => process.versions.chrome,
  electron            : () => process.versions.electron,
})
contextBridge.exposeInMainWorld('api', {
  getWorld            : () => getWorld,
  getPlayers          : () => getPlayers,
  getPlayer           : (login) => getPlayer,
  movePlayer          : (player, origin, direction) => movePlayer
})


function calcMove(origin, direction, worldSize=5) {
  var [x, y]    = origin.split('-')
  x             = parseInt(x)
  y             = parseInt(y)
  var world     = 'world_spawn'
  switch (direction) {
    case "DOWN"       : 
      x++
      break
    case "RIGHT"      : 
      y++
      break
    case "UP"         : 
      x--
      break
    case "LEFT"       : 
      y--
      break
  }
  if ((x < 0 || x > worldSize) ||
      (y < 0 || y > worldSize) ){// index out of bound
        return false;
  }
  return `${x}-${y}`
}