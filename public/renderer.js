// const calcMove = require("./shared/utils")
// import { Utils } from "./shared/utils.js"
// globals => conf
var G_verbose = 3 // 0 : mute, 3 : verbose++
var G_playerLogin = ""
var G_players = {}
var G_worldSize = 4
var G_togglePersoDisplayStatus = 'block';
var G_world = {}
G_world["world_spawn"] = {}


function displayVersion() {

    const { createApp } = Vue
    const [versionChrome,versionNode,versionElectron] = [versions.chrome(),versions.node(),versions.electron()]
    
    const message = ""
    createApp({
        data() {
            return {
                message: message,
                versionChrome: versionChrome,
                versionNode: versionNode,
                versionElectron: versionElectron
            }
        }
    }).mount('#app')
}

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

function refreshAll(newPosition = false) {
    d(`refreshAll(${newPosition})`)
    if (newPosition)
        setPlayerPosition(newPosition)
    drawAll()
}
function drawAll() {
    refreshStats()
    lifeBar()
    draw('world_spawn', G_worldSize)
}
function d(data) {
    if (G_verbose > 1 && data)
        console.log(data)
}
function refreshStats() {
    if (!G_players[G_playerLogin]) return;
    const stats_dom_id = 'stats'
    const dom = document.getElementById(stats_dom_id)
    var innerHTML = ''
    var position = getPlayerPosition()
    var world = G_players[G_playerLogin]["world"]
    var hp = G_players[G_playerLogin]["hp"]
    var avatar = G_players[G_playerLogin]["avatar"]
    var items = G_players[G_playerLogin]["item"]
    var itemsLabel = (items) ? items.length : "no item !"

    d(`refreshStats :${G_players[G_playerLogin]["position"]}`)
    d(G_players[G_playerLogin]);
    innerHTML += `
    <div class="stats" data-login="${G_playerLogin}">
        <div class="id">
            <img class="player" src="./assets/avatar/${avatar}" title="${hp}/100"><br />
            <strong>${G_playerLogin}</strong> : ${position}
            <div id="health-bar" data-total="100" data-value="${hp}">
                <div id="bar">
                    <small>${hp}/100</small>
                    <div id="hit"></div>
                </div> 
            </div>
        </div>
    </div>`
//     <div class="items">
//     <img src="./assets/chest.jpg" width=20 />
//     <div class="label">${itemsLabel}</div>
// </div>
    dom.innerHTML = innerHTML
}
function displayPersoEntry() {
    const perso_entry_dom_id = 'perso_entry'
    const dom = document.getElementById(perso_entry_dom_id)
    var innerHTML = ''
    api.getPlayers()().then((data) => {
        data.rows.forEach(row => {
            var world = row['world']
            var position = row['position']
            var selectedCSS = (row['login'] == G_playerLogin) ? ' selected' : ''
            var items = (row['items']) ? row['items'].length : '0 item!'
            //bad
            G_players[row['login']] = row

            // G_world['world_spawn'][position]['player'] += row['login']


            innerHTML += `<div class="perso${selectedCSS}" data-login="${row['login']}">`
            innerHTML += `<img class="player" src="./assets/avatar/${row['avatar']}"><br />`
            innerHTML += `<strong>${row['login']}</strong><br />`
            innerHTML += `<br />Life   : ${row['hp']}`
            innerHTML += `<br />Items  : ${items}`
            innerHTML += `<br />World : ${world}`
            innerHTML += `<br />Where : ${position}`
            innerHTML += `</div>`
        })
        dom.innerHTML = innerHTML
        var elements = document.getElementsByClassName("perso");
        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', selectPerso, false);
        }
    }).catch(e => {
        console.log(e);
    })
}

function selectPerso() {
    G_playerLogin = this.getAttribute("data-login");
    var elements = document.getElementsByClassName("perso");
    for (var i = 0; i < elements.length; i++) {
        elements[i].classList.remove("selected")
    }
    this.classList.add("selected")

    document.getElementById("persos").style.display = "none"
    docReady(drawAll)
}
function draw(dom_id, size) {
    const dom = document.getElementById(dom_id)
    var innerHTML = '<tbody>'
    d(`draw world (${dom_id}, ${size})`)
    api.getWorld()(dom_id).then((data) => {
        d("world:")
        d(data.rows)
        data.rows.forEach(row => {
            var position = row['position']
            var login = row['login']
            // if (!login) return
            delete (row['login'])
            d(` each ${position} => ${login}`)
            if (!G_world['world_spawn'][position]) {
                G_world['world_spawn'][position] = row
                if (login)
                    G_world['world_spawn'][position]["logins"] = [login]
            }
            else if (login &&
                Array.isArray(G_world['world_spawn'][position]["logins"]) &&
                G_world['world_spawn'][position]["logins"].indexOf(login) === -1)
                G_world['world_spawn'][position]["logins"].push(login)
        });
        refreshRoom(G_world['world_spawn'][G_players[G_playerLogin]['position']])
        for (var i = 0; i < size; i++) {
            innerHTML += `<tr>`
            for (var j = 0; j < size; j++) {
                innerHTML += draw_cell_html(dom_id, `${i}-${j}`)
            }
            innerHTML += `</tr>`
        }
        innerHTML += `</tbody>`
        dom.innerHTML = innerHTML
    })
    // .catch(e => {
    //     console.log(e);
    // })
}
function draw_cell_html(dom_id, position) {
    var cell = { properties: "empty" }
    if (G_world[dom_id][position])
        cell = G_world[dom_id][position]
    d(`draw_cell_html(${dom_id}, ${position}) cell:`)
    d(cell)
    if (position == G_players[G_playerLogin]["position"]){
        cell.properties += ' selected'
    }
    var output = `<td class="${cell.properties}"><div>`
    output += `<small>${position}</small>`
    if (cell.items)
        cell.items.split(' ').forEach(item => {
            imgSrc = (item.includes('.gif')) ? item : `${item}.png`
            output += `<img class="item" src="./assets/item/${imgSrc}">`
        });
    output += '</div>'
    if (Array.isArray(cell.logins))
        cell.logins.forEach(playerLogin => {
            var selected = (playerLogin == G_playerLogin) ? " selected" : ""
            if (playerLogin != null)
                output += `<img class="player${selected}" src="./assets/avatar/${playerLogin}.png">`
        });

    if (cell.mobs)
        cell.mobs.split(' ').forEach(mob => {
            output += `<img class="mob" src="./assets/mob/${mob}.png">`
        });


    output += '</td>'

    return output
}
function getPlayerPosition() {
    return (G_players[G_playerLogin]) ?
        G_players[G_playerLogin]["position"]
        : ''
}
function refreshRoom(cell=[]){
    const dom_id = "cellContent"
    const dom = document.getElementById(dom_id)
    var innerHTML = ''

    if (cell.items)
        cell.items.split(' ').forEach(item => {
            imgSrc = (item.includes('.gif')) ? item : `${item}.png`
            innerHTML += `<div>`
            innerHTML += `<img class="item" src="./assets/item/${imgSrc}">`
            innerHTML += ` <button>take</button>`
            innerHTML += `</div>`
        })
    if (cell.mobs)
        cell.mobs.split(' ').forEach(mob => {
            imgSrc = (mob.includes('.gif')) ? mob : `${mob}.png`
            innerHTML += `<div>`
            innerHTML += `<img class="item" src="./assets/mob/${imgSrc}">`
            innerHTML += ` <button>fight !</button>`
            innerHTML += `</div>`
    if (Array.isArray(cell.logins)){
        cell.logins.forEach(currentLogin => {
            var selected = (currentLogin == G_playerLogin) ? " you !" : "trade"
            innerHTML += `<div>`
            innerHTML += `<img class="player" src="./assets/avatar/${currentLogin}.png">`
            innerHTML += ` <button>${selected}</button>`
            innerHTML += `</div>`
        });
    }

    })
    dom.innerHTML = innerHTML
}
function setPlayerPosition(position) {
    d(`setPlayerPosition 2: => ${position}`)
    // alert(G_players[G_playerLogin])
     if (G_players[G_playerLogin]){
        G_players[G_playerLogin]["position"] = position
    }else {
        d(`${G_playerLogin} 404`)
    }
}
function lifeBar(damage = 0) {
    var hBar = document.getElementById("health-bar"),
        bar = document.getElementById('bar'),
        hit = document.getElementById('hit');

    var total = hBar.getAttribute("data-total"),
        value = hBar.getAttribute("data-value");

    if (value < 0) {
        d("you dead, reset");
        return;
    }
    var newValue = value - damage;
    var barWidth = (newValue / total) * 100;
    var hitWidth = (damage / value) * 100 + "%";
    d(newValue + ':barWidth:' + barWidth)
    //   hit.style.width = hitWidth;
    hit.style.width = 0;
    bar.style.width = newValue + "%";
    d(newValue + "%")
    if (value < 0) {
        d("DEAD");
    }
}
function hit(damage) {
    d(`hit ${G_players[G_playerLogin]["hp"]}`)
    G_players[G_playerLogin]["hp"] -= damage
    d(`hit ${G_players[G_playerLogin]["hp"]}`)
    // document.getElementById("health-bar").setAttribute('data-value',G_players[G_playerLogin]["hp"])
    lifeBar(damage)
    refreshStats()
}


docReady(displayVersion)
docReady(displayPersoEntry)
window.addEventListener(
    "keydown",
    (event) => {
        if (!G_players[G_playerLogin]) return
        if (event && event.defaultPrevented) {
            return; // Do nothing if event already handled
        }
        var playerPosition = getPlayerPosition()
        var direction = ''
        switch (event.code) {
            case "KeyS":
            case "ArrowDown":
                direction = 'DOWN'
                break;
            case "KeyZ":
            case "KeyW":
            case "ArrowUp":
                direction = 'UP'
                break;
            case "KeyA":
            case "KeyQ":
            case "ArrowLeft":
                direction = 'LEFT'
                break;
            case "KeyD":
            case "ArrowRight":
                direction = 'RIGHT'
                break;
        }
        var target = Utils.calcMove(playerPosition, direction)
        if (!target ||                                 // out of bound
            !target in G_world['world_spawn'])        // wall
        {
            alert(`va t acheter des doigts`)
        } else {
            d(`clear old cache ${playerPosition}`)
            // delete (G_world['world_spawn'][playerPosition])
            G_world['world_spawn'] = {}

            api.movePlayer()(G_playerLogin, playerPosition, direction).then(refreshAll).catch(e => {
                console.log(e);
            })
        }



        if (event.code !== "Tab") {
            // Consume the event so it doesn't get handled twice,
            // as long as the user isn't trying to move focus away
            event.preventDefault();
        }
    },
    true,
)


