class Utils {
  constructor() {
  }
  static calcMove(origin, direction, worldSize=5) {
    var [x, y]    = origin.split('-')
    x             = parseInt(x)
    y             = parseInt(y)
    switch (direction) {
      case "DOWN"       : 
        x++
        break
      case "RIGHT"      : 
        y++
        break
      case "UP"         : 
        x--
        break
      case "LEFT"       : 
        y--
        break
    }
    if ((x < 0 || x > worldSize) ||
        (y < 0 || y > worldSize) ){// index out of bound
          return false;
    }
    return `${x}-${y}`
  }
}

// module.exports = Utils;
