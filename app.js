const { app, BrowserWindow } = require('electron')
const path = require('node:path')
const env                         = process.env.NODE_ENV || 'development';
const ignoredNode                 = /node_modules|[/\\]\./;
const ignoredAssests              = /public\/assets|[/\\]\./; // all public folder => resources

const createWindow = () => {
    const win = new BrowserWindow({
        width                     : 800,
        height                    : 600,
        webPreferences: {
            nodeIntegration       : true,
            enableRemoteModule    : true,
            preload               : path.join(__dirname, 'src/preload.js')
        }
    })

    win.loadFile('public/index.html')

    if (env === 'development') { 
        // Open the DevTools.
        win.webContents.openDevTools()
        //auto reload
        try { 
            require('electron-reloader')(
                module, { 
                debug             : true,
                watchRenderer     : true,
                ignored           : [ignoredAssests, ignoredNode]
            }); 
        } catch (_) { console.log('Error'); }     
    }
}

app.whenReady().then(createWindow)
console.log(`node env : `);
console.log(env)

