# ktb data

## data SAMPLE
```table
|position|items                      |properties           |world      |players        |mobs                |
|--------|---------------------------|---------------------|-----------|---------------|--------------------|
|0-0     |Popo_shield  Mystic_pot    |openBottom openRight |world_spawn|aokami superT0f|                    |
|0-1     |Popo_shield                |openRight openLeft   |world_spawn|               |                    |
|0-2     |                           |openRight openLeft   |world_spawn|               |Goblin              |
|0-3     |Popo_life lucky_block.gif  |openLeft             |world_spawn|               |                    |
|1-0     |                           |openTop openBottom   |world_spawn|               |                    |
|2-0     |Popo_life                  |openTop openRight    |world_spawn|               |Goblin              |
|2-1     |Popo_life BlueSword.gif    |openLeft openRight   |world_spawn|               |                    |
|2-2     |FireSword.gif              |openLeft openRight   |world_spawn|captain        |ExpertMagicienGoblin|
|2-3     |BigSword                   |openLeft openBottom  |world_spawn|               |                    |
|3-1     |Elements                   |openTop              |world_spawn|               |Goblin              |
|3-3     |disc.gif                   |openTop openBottom   |world_spawn|               |                    |
```

## SQL CREATE :

```sql
CREATE TABLE cells (
    "position" text NULL,
    items text NULL,
    properties text NULL,
    world text NULL,
    rowid int8 NOT NULL DEFAULT unique_rowid(),
    players text NULL,
    mobs text NULL,
    CONSTRAINT cells_pkey PRIMARY KEY (rowid ASC)
);

CREATE TABLE cells_players (
	"login" text NOT NULL,
	"position" text NULL,
	world text NULL,
	CONSTRAINT cells_players_pk PRIMARY KEY ("login")
);


CREATE TABLE players (
	"login" text NULL,
	avatar text NULL,
	hp text NULL,
	items text NULL,
	"position" text NULL,
	rowid int8 NOT NULL DEFAULT unique_rowid(),
	CONSTRAINT players_pkey PRIMARY KEY (rowid ASC)
);

```

## SQL INSERT
```sql
    INSERT INTO public.cells ("position",items,properties,world,players,mobs) VALUES
	     ('0-0','Popo_shield chest Mystic_pot','openBottom openRight','world_spawn','aokami superT0f',NULL),
	     ('0-1','Popo_shield','openRight openLeft','world_spawn',NULL,NULL),
	     ('0-2','','openRight openLeft','world_spawn',NULL,'Goblin'),
	     ('0-3','Popo_life lucky_block.gif','openLeft','world_spawn',NULL,NULL),
	     ('1-0',NULL,'openTop openBottom','world_spawn',NULL,NULL),
	     ('2-0','Popo_life','openTop openRight','world_spawn',NULL,'Goblin'),
	     ('2-1','Popo_life BlueSword.gif','openLeft openRight openBottom','world_spawn',NULL,''),
	     ('2-2','FireSword.gif RainbowDiscSlower.gif','openLeft openRight','world_spawn','captain','ExpertMagicienGoblin'),
	     ('2-3','BigSword','openLeft openBottom','world_spawn','',''),
	     ('3-1','Elements','openTop','world_spawn',NULL,'Goblin'),
	     ('3-3','disc.gif','openTop openBottom','world_spawn',NULL,NULL);
```