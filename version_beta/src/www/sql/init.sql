CREATE USER 'ktb'@'%' IDENTIFIED WITH mysql_native_password AS '***';GRANT USAGE ON *.* TO 'ktb'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;CREATE DATABASE IF NOT EXISTS `ktb`;GRANT ALL PRIVILEGES ON `ktb`.* TO 'ktb'@'%';GRANT ALL PRIVILEGES ON `ktb\_%`.* TO 'ktb'@'%';
CREATE TABLE `ktb`.`user` ( `pseudo` TEXT NOT NULL , `password` TEXT NOT NULL , `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `comment` TEXT NOT NULL , `role` TEXT NOT NULL ) ENGINE = InnoDB;
ALTER TABLE `user` ADD `email` TEXT NOT NULL AFTER `password`;

INSERT INTO `user` (`pseudo`, `password`, `created`, `comment`, `role`) VALUES ('superT0f', '', CURRENT_TIMESTAMP, 'test manual', 'ADMIN');